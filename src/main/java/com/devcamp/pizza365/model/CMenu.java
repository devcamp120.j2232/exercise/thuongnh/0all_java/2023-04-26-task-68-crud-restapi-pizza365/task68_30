package com.devcamp.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "menu")
public class CMenu {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int id; 

    @Column(name = "size", unique = true, nullable = false)  // unique là duy nhất , độc nhất 
    private String size;  // size S M L

    @Column(name = "duong_kinh" )  //  nullable = false không được phép null 
    private int duongKinh;  

    @Column(name = "suong_nuong")
    private int suongNuong;  

    @Column(name = "salad")
    private int salad;  

    @Column(name = "nuoc_ngot")
    private int nuocNgot;  

    @Column(name = "don_gia")
    private int donGia;

    // phương thức khởi tạo 
    

    public CMenu(String size, int duongKinh, int suongNuong, int salad, int nuocNgot, int donGia) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suongNuong = suongNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.donGia = donGia;
    }
    public CMenu() {
    }
    public String getSize() {
        return size;
    }
    public void setSize(String size) {
        this.size = size;
    }
    public int getDuongKinh() {
        return duongKinh;
    }
    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }
    public int getSuongNuong() {
        return suongNuong;
    }
    public void setSuongNuong(int suongNuong) {
        this.suongNuong = suongNuong;
    }
    public int getSalad() {
        return salad;
    }
    public void setSalad(int salad) {
        this.salad = salad;
    }
    public int getNuocNgot() {
        return nuocNgot;
    }
    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }
    public int getDonGia() {
        return donGia;
    }
    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    

    

    

    
}
